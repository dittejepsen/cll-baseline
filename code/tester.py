import argparse
import numpy as np
import pandas as pd
import pickle as pkl

clf_types = ['bagging', 'adaboost', 'lightgbm', 'xgb', 'rf', 'extra', 'catboost']
outcomes = ['Death', 'Treatment', 'Infection', 'composite']
outlooks = [2, 5]
mdl_path = '../saved_models/'
cat_feat_fn = '../metadata/categorical_features.csv'
all_feature_names = '../metadata/model_features.csv'
# function: loading the file containing categorical variables
def get_cat_features(cat_feat_fn):
    cat_feat = pd.read_csv(cat_feat_fn,';')
    cat_feat_list = list(cat_feat.loc[:,cat_feat.iloc[0]=='c'].columns)
    return cat_feat_list

# cat encoding
def cat_encoding(x_in, cat_features, imp_mean, mdl_cols):
    common_cat = list(set(cat_features) & set(list(x_in.columns))) # check this
    x_in_new = x_in.copy()   # copy of the input
    x_in_new.loc[0, common_cat] = x_in_new.loc[0, common_cat].astype(str) # convert the categorical features
    x_dum = pd.get_dummies(x_in_new, prefix=common_cat, dummy_na=False, drop_first=False) # one-hot encoding
    # select columns match the models'
    df_temp = pd.DataFrame(0, index=np.arange(len(x_dum)), columns=mdl_cols)
    x_rem_mis = df_temp.add(x_dum) # both features from model and the input
    x_rem_mis = x_rem_mis[mdl_cols] # keep the model columns
    x_imp = imp_mean.transform(x_rem_mis)  # the output is numpy
    return x_imp

class ModelTest:

	def __init__(self, outlook, outcome, mdl_path):

		self.outlook = outlook
		self.outcome = outcome
		self.mdl_path	= mdl_path

	def evaluate(self, x):
		y_pred = 0
		df_cols = pd.read_csv(all_feature_names)
		# drop the first column (index)
		df_cols = df_cols.iloc[:, 1:]
		df_cols.set_index(['outlook', 'outcome', 'algorithm'], inplace=True)
		# load
		for clf_nm in clf_types:
			mdl_fn = self.mdl_path + '/' + str(self.outlook) + '/mdl_' + self.outcome + '_bl_' + clf_nm + '.pkl'
			# load the model
			with open(mdl_fn, "rb") as f_mdl:
				mdl = pkl.load(f_mdl)

			if clf_nm == 'catboost':
				x_in = x.fillna('NA', inplace=False)
				y = mdl.predict(x_in)
			else:
				cat_features = get_cat_features(cat_feat_fn)
				nm_tuple = (self.outlook, self.outcome, clf_nm)
				mdl_cols = list(df_cols.loc[nm_tuple, df_cols.loc[nm_tuple, :] == 1].index)
				x_cat_en = cat_encoding(x, cat_features, mdl[0], mdl_cols)
				y = mdl[1].predict(x_cat_en)


			y_pred += y
			# end loop over the models

		y_pred = np.round(y_pred / len(clf_types))

		return y_pred

def main():
	""" main function """
	# optional command line args
	parser = argparse.ArgumentParser()
	parser.add_argument('--filename', help='input data', type=str)
	parser.add_argument('--outlook', help='2- or 5-year outlook', type=int)
	parser.add_argument('--outcome', help='D(eath), T(reatment), I(nfection), c(omposite)', type=str)
	args = parser.parse_args()
	assert	args.outlook in outlooks, "error: outlook should be 2 or 5"
	assert	args.outcome in outcomes, "error: outcome is not valid"

	# load data
	data_fn = args.filename
	x = pd.read_csv(data_fn)

	# initialise model
	model_test = ModelTest(args.outlook, args.outcome, mdl_path)
	# evaluate
	y = model_test.evaluate(x)
	print(y)


if __name__ == '__main__':
	main()
